# Assorted Pi projectlets
This repo contains multiple small projects written in python for parts of the IA-level CS course i'm taking. They, by themselves, aren't very important but are collected here for maintenence and development.

## Projects
### Bf+
A failed brainfuck interpreter designed to add additional features to the language

### WorstGame.py
A game designed to be copy-pasted into a [Sense Hat Emulator](https://trinket.io/sense-hat) and ran. It uses temperature and humidity to control movement, making it incredibly hard to control and thus play in real-life situations. A 2-d matrix board of many holes and fanse emitting air of specific heat and humidity depending on location on the matrix might make a good extension project.

### RPN Language
A RPN language interpreter (my very first working interpreter) based of a stack. Runs a simple programming language, which [Plot](http://bit.ly/plot-ide) users might find similarities to:

The language is designed like this:

* `""` for letters, or plain numbers represent values
* `()` around numbers or letters represents either longer values or function names
* It uses Reverse-polish notation.
* `;` breaks the function chain, effectively starting a new statement.

Here is an example

`"Hello World!"(print)`

`12+(print);(12)(399)*(print)`

