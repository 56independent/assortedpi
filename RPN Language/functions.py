'''
This is where we handle function calls. For each function you wish to add, add a new case for it in the switch statement below.

To return a value, simply return it. 

This language works as below:

* `""` for letters, or plain numbers represent values
* `()` around numbers or letters represents either longer values or function names
* It uses Reverse-polish notation.
* `;` breaks the function chain, effectively starting a new statement and resetting the temporary memory

And as such, these are valid scripts:

"Hello World!"(print)

12+(print);(12)(399)*(print)

No functionality exists yet for handling variables
'''

def execute (arguments, functionName):
  #print("Execute params: ", arguments, functionName)
  match functionName:
    case "*":
      result = 1
      for x in arguments:
          try:
            x = int(x)
            result = result * x
          except:
            #print("Whoops!")
            pass
      return result
    case "+":
      result = 1
      for x in arguments:
          try:
            x = int(x)
            result = result + x
          except:
            #print("Whoops!")
            pass
      return result
    case "-":
      result = 1
      for x in arguments:
          try:
            x = int(x)
            result = result - x
          except:
            #print("Whoops!")
            pass
      return result
    case "/":
      result = 1
      for x in arguments:
          try:
            x = int(x)
            result = result / x
          except:
            #print("Whoops!")
            pass
      return result
    case "print":
      for arg in arguments:
        if arg != "":
          print(arg)
  
  return ""