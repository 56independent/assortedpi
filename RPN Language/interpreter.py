import main
import functions

'''
This is the central file for handling parsing and usage of this language.
'''

def run(lexed):
  values = [] 
  pending = 0

  #print(lexed)
  
  for i in range(len(lexed)): ## Go for each element in the thing
    #print(values)
    #print(lexed[i]["type"])
    if lexed[i]["type"] == "DATA_NUM" or lexed[i]["type"] == "DATA_STRING": # If its a data, add it to the stack
      main.push(lexed[i]["value"], values)
      pending += 1
    elif lexed[i]["type"] == "FUNCTION": # If its a function, handle all the previous data and then execute it
      #print("Values :", values, "Function:", lexed[i]["value"])
      final = functions.execute(values, lexed[i]["value"]) # Take the previous
      if len(values) >= pending:
        main.pop(pending, values)
      else:
        values = []
      main.push(final, values)
      pending = 0
    elif lexed[i]["type"] == "SEPERATOR":
      values = []
    

def lex(tokens):
  things = []
  print(tokens)
  
  for token in tokens:
    #print(token)
    if token != "":
      if token == ";":
        things.append({
          "type":"SEPERATOR"
        })
      if token[0] == "(":
        try:
          number = float(token[1:-1])
          things.append(
            {
              "type":"DATA_NUM",
              "value":number
            }
          )
        except:
          things.append(
            {
              "type": "FUNCTION", 
              "value": token[1:-1]
            })
      elif token[0] == "\"":
        things.append(
          {
            "type":"DATA_STRING", 
            "value": token[1:-1]
          })
      else:
        try:
          number = float(token)
          things.append(
            {
              "type":"DATA_NUM",
              "value":number
            }
          )
        except:
          things.append(
            {
              "type": "FUNCTION", 
              "value": token
            })
  return things

def tokenize(code):
  tokens = []

  tokenizerIndex = 0
  
  secondQuote = False

  '''
  1. For each character in the code
    1. Is it `(` (and not `string` being true), `"`, or `;`?
        1. Bare `(`; start the new token and wait until the `)` character before finishing by setting `bracket` to true
        1. Bare `"`; start the new token and wait until the `"` character (as long as it dosen't have a `\"` before it) by setting `string` to true
        1. bare `;`; start the new token, add `;`, and then move onto the next token
    1. If not, append the current character to the current token
  '''

  string = False
  bracket = False
  skip = False
  
  for i in range(len(code)):
    if code[i] == "(" and not string:
      tokenizerIndex += 1
      bracket = True
    elif code[i] == "\"" and (not bracket) and (not string):
      tokenizerIndex +=1
      string = True
    elif code[i] == "\"" and (not bracket) and string:
      string = False
    elif code[i] == ")" and not string:
      bracket = False
    elif code[i] == ";" and not string:
      tokens.append(code[i])
      tokenizerIndex += 1
      skip = True
    elif not string and not bracket:
      tokenizerIndex += 1

    while tokenizerIndex >= len(tokens):
      tokens.append("")
    
    if not skip:
      tokens[tokenizerIndex] += code[i]

    skip = False

  return tokens ## We now have a list of valid tokens

def inputCode():
  # handle input; if this was a script on a computer, we'd allow reading files, but for now we're going for a basic interface until later

  code = ""
  code = input("Code: ")

  if not code:
    print("Code not provided. Using template statement.")
    return "\"Hello World!\"(print);12+(print);(12)(399)*(print)"
  
  return code



code = inputCode()
tokens = tokenize(code)
lexed = lex(tokens)
run(lexed)

#print(tokens)
#print(lexed)