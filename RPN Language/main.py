#The extension here is to add to it with a queue and/or turn it into a reverse polish notation interpreter. So someone puts in 4+5-6 and your program turns that into 45+6-, then pushes 4 then 5 onto the stack, pops them, adds them, pushes the result onto the stack, then 6, pops the two values and subtracts them.

# If you want to see the code for handling the language, visit tokenizer.py. Defining new functions is available in functions.py.

## -- Stack functions

def push(value, list):
  if type(value) is list:
    for i in range(len(value)):
      list.append(value[i])
  else:
    list.append(value)

  return list


def pop(value, list):
  # For these functions, value can either be a list or number. Here, we only care about the length or number.

  try:
    for i in range(value):
      list.pop()
  except:
    for i in range(len(value)):
      list.pop()

  return list


def isEmpty(list):
  return len(list) == 0

## -- Queue functions

def pushQueue(value, list): ## Add a value to the list
  list.append(value)
  return list # I would draw this as a single line but i'm not sure if i'd just be returning the return value of the append function, which would just be True.

def pullQueue(list): ## Gives us the value at the front of the queue
  value = list[0]
  list.pop(0)
  
  return [list, value]

def peekQueue(value, list): ## Returns the value at both ends of the queue
  return [list[0], list[-1]]

def test():
  stack = [23, 43, "Me", "LMAO", 2 ^ 100]
  print(stack)

  stack = push("23", stack)
  print(stack)

  stack = push(["43", 74], stack)
  print(stack)

  stack = pop(3, stack)
  print(stack)

  stack = pop(["32", 4, 3, 2, 1], stack)
  print(stack)


'''
Well, i've been instructed to make a Reverse-Polish notation thing in Python.

First, i'll need to start with abstracting away a stack from a list, making a few base functions. Then, using these functions, i can begin work on a second file for interpreting the language.

The language is designed like this:

* `""` for letters, or plain numbers represent values
* `()` around numbers or letters represents either longer values or function names
* It uses Reverse-polish notation.
* `;` breaks the function chain, effectively starting a new statement.

Lets come up with examples:

`"Hello World!"(print)`

`12+(print);(12)(399)*(print)`

Ok, that's kind of ugly but works. Now, as for the stack-based part.

I'll have these functions:

* `pop()` - Returns a value from the top of the stack and removes it
* `peek()` - Returns the value at the top of the stack
* `push()` - Adds a value to the top of the stack

Each of these can either take a list or number at the front; these represent the values or number of values to add or remove. The second value is always the array.

Now then, as for the language parser. What i'll be doing is having a two-fold process. The first will be the conversion of a raw string into tokens; these are the individual values and strings. The next stage will be sending it to a central function for handling. The way it's built is to be extensible; there are a series of switch and case statements that allow one to choose what functions implement. 
'''


