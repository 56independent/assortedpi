'''
Main things

>	Increment the data pointer by one (to point to the next cell to the right).
<	Decrement the data pointer by one (to point to the next cell to the left).
+	Increment the byte at the data pointer by one.
-	Decrement the byte at the data pointer by one.
.	Output the byte at the data pointer.
,	Accept one byte of input, storing its value in the byte at the data pointer.
[	If the byte at the data pointer is zero, then instead of moving the instruction pointer forward to the next command, jump it forward to the command after the matching ] command.
]	If the byte at the data pointer is nonzero, then instead of moving the instruction pointer forward to the next command, jump it back to the command after the matching [ command.[a]

Later extensions:

| **Syntax** | **Description** | **Example** |
| --- | --- | --- |
| `( )` | Defines a function, which acts like a macro in c | `copy([>+>+<<-]>>[-<<+>>])`, called `>>+copy` (names cannot be subsets!!!) |
| `{ }` | Names the cell we're in and when called later, moves us to that cell. | `>{counter}`, called `{counter}` |
| `" "` | Defines a string, which will then be placed into the cells from current pointer to the end of input. The beggining and end are in `{beggining}` and `{end}` |     |
| `* *` | Defines a block of code which does a meaningful thing | `*[->+<]*` |
| `+10` | Repeats plus 200 times into the current pointer location | `+10` to repeat an instruction, `*[->+<]>*10` to repeat a block of code |
'''

'''
Algorithm design:

1. Take the program input, either from a file (regex `.+\..+` should return true), argv[2], or input statement
1. Break it down into "morphemes" as from the above, disposing all unparsable morphemes. 
1. Parse each morpheme as necessary
    1. Print out anything anything with the  `.`
'''

import sys
import re

environement = { ## Kept as a dictionary for easier debugging
    "code": code,
    "lexed": [],
    "tape": [],
    "stack": [],   
}

if len(sys.argv) > 1:
    argument = sys.argv[1]

    result = bool(re.match(".+\..+", argument)) and len(argument) > 0
    if not result:
        environment.code = argument
    else:
        with open(argument) as file:
            environment.code = file.read()
else:
    environment.code = input("Enter your bf program: ")

def lex(code):
    tokens = ["<", ">", "+", "-", ".", ",", "[", "]"]
    lexedArray = []

    # We find the longest possible match for code[i], and then save that.
    for i in range(len(code)):
        potentialChars = []
        for token in tokens: # https://chat.openai.com/share/37624324-e820-4115-95b6-cb6d55907939 was used
            if code.startswith(token, i):
                potentialChars.append(token)
        '''for o in range(len(tokens)):
            pass
            if len(tokens[o]) > 1:
                pass
                for j in range(len(tokens[o])):
                    if not code[i+j] == tokens[o][j]:
                        break # We did not find a match
                    if j == len(tokens[o]):
                        i += j
                        potentialChars.append(tokens[o])
                        
            elif code[i] == tokens[o]:
                potentialChars.append(tokens[o])'''
                
        
                
        print(potentialChars)
        if potentialChars: lexedArray.append(max(potentialChars, key=len)) # Take the longest character from the potential characters
    
    return lexedArray
    
def execute(environment): ## Execute from the environment
    pass

print(environment.code)

environment.lexed = lex(environment.code)

print(environment.lexed)

execute(environemnt)

