# To run: https://trinket.io/sense-hat

# To play, change the humidity and heat >:-)

from sense_hat import SenseHat
import time
import random

s = SenseHat()

def game():
    green = (0, 255, 0)
    yellow = (255, 255, 0)
    blue = (0, 0, 255)
    red = (255, 0, 0)
    white = (255,255,255)
    nothing = (0,0,0)
    pink = (255,105, 180)

    snakes = [ # Each snake is defined as their direction and head-location. 
       {
        "direction": "up",
        "location": 50 # Defined as numbers of pixels since start. 
       }
    ]

    difficulty = 0 # Number of snakes

    start_time = time.time()

    # Your program logic goes here

    # Calculate the elapsed time since the program started

    old_time = 0

    def drawSnakes(snakes): ## Takes in the snakes and returns a table filled with snakes.
        G = green
        Y = yellow
        B = blue
        O = nothing

        blankBoard = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
        
        if difficulty == 0: # TODO: Make this workaround more gracious
            return blankBoard
        
        for i in range(len(snakes)):
            try: # Replace this code with a mathematical expression
                print(snakes[i]["direction"])
                print(snakes[i]["location"])
                if snakes[i]["direction"] == "up":
                    blankBoard[snakes[i]["location"]] = red
                    blankBoard[snakes[i]["location"]+8*1] = blue
                    blankBoard[snakes[i]["location"]+8*2] = blue
                elif snakes[i]["direction"] == "down":
                    blankBoard[snakes[i]["location"]] = red
                    blankBoard[snakes[i]["location"]-8*1] = blue
                    blankBoard[snakes[i]["location"]-8*2] = blue
                elif snakes[i]["direction"] == "left": # FIXME: Snakes will wrap!
                    blankBoard[snakes[i]["location"]] = red
                    blankBoard[snakes[i]["location"]+1] = blue
                    blankBoard[snakes[i]["location"]+2] = blue
                elif snakes[i]["direction"] == "right":
                    blankBoard[snakes[i]["location"]] = red
                    blankBoard[snakes[i]["location"]-1] = blue
                    blankBoard[snakes[i]["location"]-2] = blue
            except Exception as e: # TODO: Add better exception handling for the more likely and normal errors
                print("Error: " + str(e))
        
        return blankBoard

    def snakeStep(snakes): # Take snakes, move them one place forwards, remove snakes entirely off the board (location < -8*3, > 8*9), and spawn new snakes according to difficulty.
        def newSnake():
            direction = random.choice(["up", "down", "left", "right"])
            
            location = 0
            
            if direction == "up":
                location = random.randint(8*7+1, 8*8)
            elif direction == "down":
                location = random.randint(8*0, 8*1)
            elif direction == "left":
                location = random.randint(1, 8)*8+7
            elif direction == "right":
                location = random.randint(1, 8)*8
            
            return {"direction": direction, "location": location}
                


        # Move forwards
        for i in range(len(snakes)):
            if snakes[i]["direction"] == "up":
              snakes[i]["location"] -= 8
            elif snakes[i]["direction"] == "down":
              snakes[i]["location"] += 8
            elif snakes[i]["direction"] == "left": # FIXME: Snakes will wrap!
              snakes[i]["location"] -= 1
            elif snakes[i]["direction"] == "right":
              snakes[i]["location"] += 1

        # Remove lost snakes and respawn them as neccesary
        for i in range(len(snakes)):
            location = snakes[i]["location"]
            if location < -8*3 or location > 8*9:
                snakes[i] = newSnake()
                
        if len(snakes) < difficulty:
            snakes.append(newSnake())
        return snakes

    def calcPosition(temp, humidity): # Given temprature and humidity, find 
        while temp > 50:
            temp = temp-10
        while temp < 0:
            temp = temp+10
            
        while humidity > 80:
            humidity -= 10
        while humidity < 20:
            humidity += 10
            
        posTemp = 7 - (temp / 50) * 7 # 1  0-30 is the range of temp
        posHumidity = ((humidity-20)/60)*7 # 20-80 is the humidity  
        
        posTemp = max(0, min(7, posTemp))
        posHumidity = max(0, min(7, posHumidity))
        
        print(posTemp, posHumidity)
        
        return posTemp * 8 + posHumidity

    while True:
        elapsed_time = int(time.time() - start_time)
        
        if elapsed_time > old_time:
            old_time = elapsed_time
            
            snakes = snakeStep(snakes)
            board = drawSnakes(snakes)
            
            position = int(calcPosition(s.temp, s.humidity))
            
            print(position)
            
            if board[position] != nothing:
                return elapsed_time
            
            board[position] = green
        
            s.set_pixels(board)
            
            if elapsed_time%10 == 0:
                difficulty += 1


while True: # Main loop
    score = game()
    print("You died! With a score of " + str(score))
    s.show_message("You died with " + str(score) + " points")
    
    
    
    
